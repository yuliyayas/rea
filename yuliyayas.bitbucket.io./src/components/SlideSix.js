import React from 'react';

const SlideFive = () => {
  return(
    <div className="row" id="retail">
        <div className="slide-6-left">
        <p className="top-margin-6 p-width-60">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Wisi enim ad minim veniam, quis nostrud exerci tationi ullamcorper suscipit lobortis nisl ut aliquip ex eat commodo consequat.</p>
        <br />
        <div className="bottom">
          <a href={require(`../pdfs/141 E Houston_Website.pdf`)} download="test-pdf-file">
          <button className="button button-black same-line"> DOWNLOAD FLOOR PLANS</button></a>
          <img className="arrow-down-download"
          alt=""
          src={require(`../imgs/arrow-down.png`)}
          />
        </div>
        <br/>
        <br/>
        <br/>
        <br/>

        </div>
        <div className="slide-6-right">
        <p className="italic">Retail Space</p>
        <h1 className="h1-margin">BRIGHT</h1>
        <h1 className="h1-margin">OUT IN</h1>
        <h1 className="h1-margin">THE OPEN</h1>

        </div>

        <div className="slide-3-middle">
          <div className="right-left-column">

          </div>
          <div className="middle-image-gallery">
          <div className="carousel-wrapper">
                <span id="img-1"></span>
                <span id="img-2"></span>
                <span id="img-3"></span>
                <div className="carousel-item img-1">

                  <a className="arrow arrow-prev" href="#img-3"></a>
                  <a className="arrow arrow-next" href="#img-2"></a>
                </div>

                <div className="carousel-item img-2">

                  <a className="arrow arrow-prev" href="#img-1"></a>
                  <a className="arrow arrow-next" href="#img-3"></a>
                </div>

                <div className="carousel-item img-3">

                  <a className="arrow arrow-prev" href="#img-2"></a>
                  <a className="arrow arrow-next" href="#img-1"></a>
                </div>
              </div>

          </div>
          <div className="right-left-column">
          </div>

        </div>


        <div className="slide-6-left-1 padding-slide5-1">
          <img className="left-img-slide6"
          alt=""
          src={require(`../imgs/photo-9.png`)}
          />
          <br/>
          <br/>
          <br/>
          <p className="p-width-60">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.</p>
          <br />
        </div>
        <div className="slide-6-right-1">
          <img className="right-img-slide6"
          alt=""
          src={require(`../imgs/photo-8.png`)}
          />
          <br />
          <br />
          <br />
          <h2>80’ OF FRONTAGE ON HOUSTON</h2>
          <p className="p-width-60">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.</p>
          <p className="p-width-60">
            Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.
          </p>
        </div>
        <div className="menu">
        </div>
      </div>
  )
}

export default SlideFive;

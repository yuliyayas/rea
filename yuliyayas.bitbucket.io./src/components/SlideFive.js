import React from 'react';

const SlideFive = () => {
  return(
    <div className="row" id="office">
        <div className="slide-5-left padding-slide3">
          <p className="italic">Office Space</p>
          <h1 className="h1-margin">A NEW</h1>
          <h1 className="h1-margin">FRAME OF</h1>
          <h1 className="h1-margin">WORK</h1>

        </div>
        <div className="slide-5-right">
          <p className="top-margin">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Wisi enim ad minim veniam, quis nostrud exerci tationi ullamcorper suscipit lobortis nisl ut aliquip ex eat commodo consequat.</p>
          <br />
          <div className="top-margin-1 bottom">
            <a href={require(`../pdfs/141 E Houston_Website.pdf`)} download="test-pdf-file">
            <button className="button button-black same-line"> DOWNLOAD FLOOR PLANS</button></a>
            <img className="arrow-down-download"
            alt=""
            src={require(`../imgs/arrow-down.png`)}
            />
          </div>
          <br/>
          <br/>
          <br/>
        </div>
        <div className="slide-3-middle">
          <div className="right-left-column">

          </div>
          <div className="middle-image-gallery">
          <div className="carousel-wrapper">
                <span id="image-1"></span>
                <span id="image-2"></span>
                <span id="image-3"></span>
                <div className="carousel-item image-1">

                  <a className="arrow arrow-prev" href="#image-3"></a>
                  <a className="arrow arrow-next" href="#image-2"></a>
                </div>

                <div className="carousel-item image-2">

                  <a className="arrow arrow-prev" href="#image-1"></a>
                  <a className="arrow arrow-next" href="#image-3"></a>
                </div>

                <div className="carousel-item image-3">

                  <a className="arrow arrow-prev" href="#image-2"></a>
                  <a className="arrow arrow-next" href="#image-1"></a>
                </div>
              </div>

          </div>
          <div className="right-left-column">
          </div>

        </div>
        <div className="slide-5-left-1 padding-slide5-1">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
          <h1 className="h1-margin">AMMENITIES</h1>

          <img className="imgs-slide5"
          alt=""
          src={require(`../imgs/photo-7.png`)}
          />
          <br/>
          <br/>
          <br/>
          <h2>VIEWS</h2>
          <p className="p-width-60">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.</p>
          <br />
          <img className="img-rooftop"
          alt=""
          src={require(`../imgs/photo-5.png`)}
           />
          <h2>ROOFTOP DECK</h2>
          <p className="p-width-60">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.</p>
          <br />
          <br />
          <br />
          <br />
          <br />
        </div>
        <div className="slide-5-right-1">
          <img className="right-img-slide5"
          alt=""
          src={require(`../imgs/photo-6.png`)}
           />
          <br />
          <h2>HOUSTON ALLEY</h2>
          <p className="p-width-70">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.</p>
          <p className="p-width-70">
            Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod.
          </p>

        </div>
        <div className="menu">
        </div>
      </div>
  )
}

export default SlideFive;

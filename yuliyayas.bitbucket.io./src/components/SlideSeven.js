import React from 'react';

const SlideSeven = () => {
    return(
      <div>
        <div className="slide-7-left-top" id="availability">
          <p className="italic margin-table">Available Space</p>
          <h1 className="h1-margin margin-table">AVAILABILITIES</h1>
        </div>
        <div className="row">
        <div className="left">
        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 8
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 7
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 6
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 5
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 4
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 3
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 2
          </div>
        </div>

        <div className="row row-h row-height margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          LEVEL 1
          </div>
        </div>

        <div className="row row-h row-height-g margin-table">
          <div className="underline-1 slide-7-11">
            <div className="bold">5,000 RSF</div>
          </div>
          <div className="underline-1 slide-7-12">
            OFFICE SPACE
          </div>
          <div className="underline-1 slide-7-13">
            <div className="bold">DOWNLOAD PDF</div>
          </div>
          <div className="underline-2 slide-7-14">
          GROUND
          </div>
        </div>
        </div>
        <div className="right">
          <img id="building-outline"
          src={require(`../imgs/Stacking-plan.png`)}
          />
        </div>
        </div>


        <div className="row">
        <div className="grid-three-border-1 bottom">
        <a href={require(`../pdfs/141 E Houston_Website.pdf`)} download="test-pdf-file">
        <button className="button button-black same-line"> ALL FLOOR PLANS</button></a>
        <img className="arrow-down-download"
        alt=""
        src={require(`../imgs/arrow-down.png`)}
        />
        </div>
        <div className="grid-three-1-2 bottom">
        <a href={require(`../pdfs/141 E Houston_Website.pdf`)} download="test-pdf-file">
        <button className="button button-black same-line"> ALL TEST FITS</button></a>
        <img className="arrow-down-download"
        alt=""
        src={require(`../imgs/arrow-down.png`)}
        />
        </div>
        <div className="grid-three-border bottom">
        <a href={require(`../pdfs/141 E Houston_Website.pdf`)} download="test-pdf-file">
        <button className="button button-black same-line"> STACKING PLAN</button></a>
        <img className="arrow-down-download"
        alt=""
        src={require(`../imgs/arrow-down.png`)}
        />

        </div>
        </div>
      </div>
    )
}

export default SlideSeven;

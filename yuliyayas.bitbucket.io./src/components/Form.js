import React from 'react';

class Form extends React.Component{
  constructor(){
    super()
    this.state={
      first_name: "",
      last_name: "",
      email: "",
      phone :"",
      message:""
    }
  }

  handleChange = (e) => {
     e.preventDefault();
     const name = e.target.name;
     const value = e.target.value;
     this.setState(prevState => {
       return { [name]: value };
     }, () => console.log(this.state));
   };

   handleSubmit = (e) => {
     e.preventDefault();
     alert("submitted!")
   }

  render(){
    return(
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-left grey">
            <input placeholder="FIRST NAME" name="first_name"className="form-text" onChange={this.handleChange}/>
            <input placeholder="EMAIL" name="email" className="form-text" onChange={this.handleChange}/>
          </div>
          <div className="form-right grey">
            <input placeholder="LAST NAME" name="last_name" className="form-text" onChange={this.handleChange}/>
            <input placeholder="PHONE" name="phone" className="form-text" onChange={this.handleChange}/>
          </div>
          <textarea rows="10" className="form-message" name="message" placeholder="MESSAGE" onChange={this.handleChange}/>
          <button type="submit" className="button button-center">SEND</button>
        </form>
    </div>

    )
  }
}

export default Form;


/*
<form>
<div>
<div className="form-left grey">
<input className="form-text">FIRST NAME</input>
<input className="form-text">EMAIL</input>
</div>
<div className="form-right grey">
<input className="form-text">LAST NAME</input>
<input className="form-text">PHONE</input>
</div>
<h4 className="grey form-message">MESSAGE</h4>
<button>Submit</button>

</div>
</form>
</div>*/

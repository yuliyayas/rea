import React from 'react';
import BackToTop from './BackToTop'
const SlideTwo = () => {
  return(
    <div>
    <div>
    <div className="row">
        <div className="slide-2-left padding-slide2">
          <img
          alt=""
          src={require(`../imgs/building.png`)}
          width="70%"/>
          <br />
          <p className="text-p">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Wisi enim ad minim veniam, quis nostrud exerci tationi.</p>
          <br/>
          <br/>
          <div className="bottom">
            <a href="#availability">
            <button className="button button-black same-line"> BUILDING SPECIFICATIONS</button>
            </a>
            <img className="arrow-down-download"
            alt=""
            src={require(`../imgs/arrow-down.png`)}
            />
          </div>
          <br />
        </div>
        <div className="slide-2-right padding-right-s2">

          <p className="italic">The Architect</p>
          <h1 className="h1-margin">ITS A</h1>
          <h1 className="h1-margin">MATTER</h1>
          <h1 className="h1-margin">OF FORM</h1>
          <img className="image-slide-2"
          alt=""
          src={require(`../imgs/video.png`)}
          />
          <div className="padding-s2-p">
          <p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
          <br/>
          <p>Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
          </div>
        </div>
        <div className="menu">
        </div>
        <BackToTop />
      </div>
    </div>
    </div>
  )
}

export default SlideTwo;

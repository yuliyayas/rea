import React from 'react';

const BackToTop = () => {
  return(
      <div className="row">
      <div className="back-left">
          <p className="italic" onClick={() => {
              document.body.scrollTop = 0;
              document.documentElement.scrollTop = 0;
          }}>
          Back to Top</p>
      </div>
      <img className="img-arrow" onClick={() => {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
      }}
      alt=""
      src={require(`../imgs/arrow-up.png`)}
       />
      </div>
  )
}

export default BackToTop;

import React from 'react';

class Header extends React.Component{



  render(){
  return(
    <div>
    <div className="row">
        <div className="header" id="header">
            <img id="logo"
            alt=""
            src={require(`../imgs/logo.png`)}
            />
        </div>
        <div className="menu">

              <nav role='navigation fixed-menu'>
              <div id="menuToggle">

                <input type="checkbox" />

                <span></span>
                <span></span>
                <span></span>


                <ul id="menu">
                  <a href="#overview"><li>OVERVIEW</li></a>
                  <a href="#location"><li>LOCATION</li></a>
                  <a href="#facts"><li>FACTS</li></a>
                  <a href="#office"><li>OFFICE</li></a>
                  <a href="#retail"><li>RETAIL</li></a>
                  <a href="#availability"><li>AVAILABILITY</li></a>
                  <a href="#contacts"><li>CONTACT</li></a>
                </ul>
              </div>
              </nav>
        </div>
      </div>
    </div>

  )
}
}

export default Header;


/*
this is to hide the bar on scroll - change y coordinates
    componentDidMount(){
      var scrollpos = function(){return window.scrollY};
      window.addEventListener("scroll", function(){
        const header = document.getElementById("header")
        if (scrollpos() < 120){
          header.style.display = "show"
          console.log("position in show", scrollpos());
        }
        else{
          header.style.visibility = "hidden"
          console.log("position in hidden", scrollpos());
        }
      })
    }
    */

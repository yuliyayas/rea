import React from 'react';
import GoogleMap from './GoogleMap'

const Map = () => {
  return(
    <div className="row">
      <div className="slide-map-left">
        <div className="map-style">
        <GoogleMap />
        </div>
      </div>
      <h4>TRANSPORTATION</h4>
      <div className="slide-map-right">
        <div className="p-width-60">
        <p>Lorem ipsum dolor sit amet, consec- tetuer adiping elit, sed diam nonummy nibh euismod.</p>
        <p>Nearby trainlines include:</p>
        </div>
        <img className="img-trains"
        alt=""
        src={require(`../imgs/Subway.png`)}
        />
        <br />
        <br />
        <div className="grey">
          <br />
          <h4 onClick={() => alert("change map view to restaurants")}>RESTAURANTS</h4>
          <h4 onClick={() => alert("change map view to cafes")}>CAFE’S</h4>
          <h4 onClick={() => alert("change map view to hotels")}>HOTELS</h4>
          <h4 onClick={() => alert("change map view to entertainment")}>ENTERTAINMENT</h4>
          <h4 onClick={() => alert("change map view to culture")}>CULTURE</h4>
        </div>

      </div>

    </div>
  )
}

export default Map;

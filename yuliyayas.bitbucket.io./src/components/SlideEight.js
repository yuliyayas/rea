import React from 'react';
import Form from './Form'
import BackToTop from './BackToTop'

const SlideEight = () => {
  return(
    <div className="row">
          <div className="slide-8-left"  id="contacts">
            <img className="img-glasses"
            alt=""
            src={require(`../imgs/photo-10.png`)}/>
            <Form />
          </div>
          <div className="slide-8-right">
            <div className="bottom-margin-right">
            <p className="italic">Contact</p>
            <h1 className="h1-margin">DROP US</h1>
            <h1 className="h1-margin">A LINE</h1>
            </div>
            <div className="slide-8-left-1">
              <img
              width="70%"
              alt=""
              src={require(`../imgs/Gragory-logo.png`)}
              />
              <p>OFFICE INQUIRIES</p>
              <p>Gregory Kraut</p>
              <p>646 665 4508</p>
              <p>gkraut@kpropertygroup.com</p>
            </div>
            <div className="slide-8-right-1">
              <img
              alt=""
              width="70%"
              id="padding-align"
              src={require(`../imgs/East-End-logo.png`)}
              />
              <p>RETAIL INQUIRIES</p>
              <p>Richard Skulnik</p>
              <p>212 750 6565</p>
              <p>rskulnik@ripcony.com</p>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div className="button-padding bottom">
            <a href={require(`../pdfs/141 E Houston_Website.pdf`)} download="test-pdf-file">
            <button className="button same-line"> ALL DOWNLOADS</button></a>
            <img className="arrow-down-three"
            alt=""
            src={require(`../imgs/arrow-down.png`)}
            />
            </div>
            <BackToTop />
          </div>

      </div>
  )
}

export default SlideEight;



import React from 'react';

const SlideFour = () => {
  return(
    <div className="row">
      <div className="left-column">

      </div>
      <div className="middle-column">
        <img className="long-img"
        alt=""
        src={require(`../imgs/photo-4.png`)}
        />
        <img className="long-img-hidden"
        alt=""
        src={require(`../imgs/photo-4.png`)}
        />
      </div>
      <div className="right-column"  id="facts">
        <div className="margin-s4 padding-slide1">
        <p className="italic">Facts and Figures</p>
        <h2 className="h1-margin">FACTS</h2>
        <h2 className="h1-margin">AND </h2>
        <h2 className="h1-margin">FIGURES</h2>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div className="grid-container">
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date" className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
          <div className="grid-item">
            <p className="italic">Facts and Figures</p>
            <h1 className="date">12</h1>
          </div>
        </div>
      </div>

      <div className="menu">

      </div>
    </div>
  )
}

export default SlideFour;

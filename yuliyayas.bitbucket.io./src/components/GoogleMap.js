import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => (
  <div style={{
    position: 'relative', color: 'red',
    height: 40, width: 150, fontWeight: "bold", fontSize: "14px"

  }}>
    {text}
  </div>
);


class GoogleMap extends Component {
  static defaultProps = {
    center: {lat: 40.7232389, lng: -73.9920373},
    zoom: 16
  };

  render() {
    const styles = {
    width: "600px",
    height: "60px"
  };
    return (
      <div style={{width: "80%", height: "500px", paddingLeft: "10%"}}>
       <GoogleMapReact
        defaultCenter={this.props.center}
        defaultZoom={this.props.zoom}
      >
        <AnyReactComponent
          lat={40.7232389}
          lng={-73.9920373}
          text={'. 141 E Houston Street'}
        />
      </GoogleMapReact>
      </div>
    );
  }
}
 export default GoogleMap

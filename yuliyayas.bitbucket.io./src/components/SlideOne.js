import React from 'react';

const SlideOne = () => {
  return(
    <div>
    <div className="row" id="overview">
        <div className="slide-1-left padding-slide1">
          <p className="italic">An Overview</p>
          <br/>
          <h1 className="h1-margin">FRAMING</h1>
          <h1 className="h1-margin">A NEW</h1>
          <h1 className="h1-margin">VISION</h1>
          <br/>
          <br/>
          <br/>
          <p className="text-p">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Wisi enim ad minim veniam, quis nostrud exerci tationi ullamcorper suscipit lobortis nisl ut aliquip ex eat commodo consequat.</p>
          <br/>
          <br/>
          <a href="#availability">
          <button className="button"> VIEW AVAILABILITIES</button>
          </a>
        </div>
        <div className="slide-1-right">
          <img className="image-slide-1"
          alt=""
          src={require(`../imgs/photo-1.png`)}
          />
        </div>
        <div className="menu border-left">
        </div>
      </div>
    </div>

  )
}

export default SlideOne;

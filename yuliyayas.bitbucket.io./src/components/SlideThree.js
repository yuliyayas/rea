import React from 'react';

const SlideThree = () => {
  return(
    <div className="row" id="location">
        <div className="slide-3-left-p padding-slide3">
          <p className="italic">The Location</p>
          <br/>
          <h1 className="h1-margin">BEYOND</h1>
          <h1 className="h1-margin">THE</h1>
          <h1 className="h1-margin">EVERYDAY</h1>
          <br/>
          <br/>
          <br/>
        </div>
        <div className="slide-3-right">
          <p className="top-margin">Lorem ipsum dolor sit amet, consectetuer adiping elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Wisi enim ad minim veniam, quis nostrud exerci tationi ullamcorper suscipit lobortis nisl ut aliquip ex eat commodo consequat.</p>
        </div>
        <div className="slide-3-middle">

          <div className="middle-image-gallery">
          <div className="carousel-wrapper">
                <span id="item-1"></span>
                <span id="item-2"></span>
                <span id="item-3"></span>
                <div className="carousel-item item-1">

                  <a className="arrow arrow-prev" href="#item-3"></a>
                  <a className="arrow arrow-next" href="#item-2"></a>
                </div>

                <div className="carousel-item item-2">

                  <a className="arrow arrow-prev" href="#item-1"></a>
                  <a className="arrow arrow-next" href="#item-3"></a>
                </div>

                <div className="carousel-item item-3">

                  <a className="arrow arrow-prev" href="#item-2"></a>
                  <a className="arrow arrow-next" href="#item-1"></a>
                </div>
              </div>

          </div>


        </div>
        <div className="slide-3-left padding-slide3-1">
          <img className="left-img"
          alt=""
          src={require(`../imgs/photo-2.png`)}
          />
        </div>
        <div className="slide-3-right-1">
          <img className="right-img"
          alt=""
          src={require(`../imgs/photo-3.png`)}
          />
          <div className="padding-slide3-1">
            <h1 className="h1-margin-1">“EVERYBODY OUGHT TO</h1>
            <h1 className="h1-margin-1">HAVE A LOWER EAST SIDE </h1>
            <h1 className="h1-margin-1">IN THEIR LIFE”</h1>
            <br/>
            <p className="italic">— Irving Berlin</p>
            <br/>
            <br/>
            <br/>
          </div>

        </div>
        <div className="menu">
        </div>
        
      </div>
  )
}

export default SlideThree;

import React from 'react';

const Footer = () => {
  return(
    <div id="footer" className="row">
      <div className="footer-left">
        <p className="footer-text-color">© 2018 - ALL RIGHTS RESERVED</p>
      </div>
      <div className="footer-right">
        <p className="footer-text-color">141 EAST HOUSTON STREET, NEW YORK NY 10001</p>
      </div>
    </div>
  )
}

export default Footer;

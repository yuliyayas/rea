import React, { Component } from 'react';
import Header from './components/Header';
import SlideOne from './components/SlideOne';
import SlideTwo from './components/SlideTwo';
import SlideThree from './components/SlideThree';
import Map from './components/Map';
import SlideFour from './components/SlideFour';
import SlideFive from './components/SlideFive';
import SlideSix from './components/SlideSix';
import SlideSeven from './components/SlideSeven';
import SlideEight from './components/SlideEight';
import Footer from './components/Footer';
import BackToTop from './components/BackToTop';


class App extends Component {

  render() {
    return (
      <div>
        <div>
        <Header />
        <div className="border-right">
        <SlideOne />
        <SlideTwo />
        <SlideThree />
        <Map />
        <BackToTop />
        <SlideFour />
        <SlideFive />
        <BackToTop />
        <SlideSix />
        <BackToTop />
        <SlideSeven />
        <BackToTop />
        <SlideEight />
        </div>
        <Footer />
        </div>

      </div>

    );
  }
}

export default App;
